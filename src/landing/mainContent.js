import React from 'react';
export default ({ title }) => {
    const renderTitleToLetters = (title) => {
        return title.split('').map(letter => {
            return <h1>{letter}</h1>
        })
    }
    return (<div className="content-container">
        {[1, 2, 3, 4].map((elem, index) => {
            return (<div className="content-tile" style={index % 2 === 0 ? { flexDirection: "row-reverse" } : null}>
                <div className="title">
                    <div className="letter-container">
                        {renderTitleToLetters(title)}
                    </div>
                    <div className="slug">
                        This is a short text content to give a brief description.
                    </div>
                    <div className="details">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum cursus nibh vitae facilisis lacinia. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec placerat congue lacinia. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
            </div>
                </div>
                <div className="image">
                    <img src="https://imagejournal.org/wp-content/uploads/bb-plugin/cache/23466317216_b99485ba14_o-panorama.jpg" />
                </div>
            </div>)
        })
        }
    </div >)
}