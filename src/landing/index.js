import React, {Component} from 'react';
import MainContent from "./mainContent"
import "./style.css";
export default class Landing extends Component{
    constructor(){
        super();
        this.state={
            landing:true
        }
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({landing:false})
        },2700)
    }
    render(){
        return(
            <div className="wrapper">
            {
            this.state.landing
                ?<div className="landing-container">
                    <h1>Testing</h1>
                </div>
                :<MainContent title={"David"}/>    
            }

            </div>
        )
    }
}